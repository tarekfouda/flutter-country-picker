import 'package:flutter/material.dart';

import 'country.dart';
import 'country_list_theme_data.dart';
import 'country_localizations.dart';
import 'country_service.dart';
import 'res/country_codes.dart';
import 'utils.dart';

class CountryListView extends StatefulWidget {
  /// Called when a country is select.
  ///
  /// The country picker passes the new value to the callback.
  final ValueChanged<Country> onSelect;

  final String fontFamily;

  final Widget Function(Function(String query)) buildSearch;

  /// An optional [showPhoneCode] argument can be used to show phone code.
  final bool showPhoneCode;

  /// An optional [exclude] argument can be used to exclude(remove) one ore more
  /// country from the countries list. It takes a list of country code(iso2).
  /// Note: Can't provide both [exclude] and [countryFilter]
  final List<String>? exclude;

  /// An optional [countryFilter] argument can be used to filter the
  /// list of countries. It takes a list of country code(iso2).
  /// Note: Can't provide both [countryFilter] and [exclude]
  final List<String>? countryFilter;

  /// An optional [favorite] argument can be used to show countries
  /// at the top of the list. It takes a list of country code(iso2).
  final List<String>? favorite;

  /// An optional argument for customizing the
  /// country list bottom sheet.
  final CountryListThemeData? countryListTheme;

  /// An optional argument for initially expanding virtual keyboard
  final bool searchAutofocus;

  /// An optional argument for showing "World Wide" option at the beginning of the list
  final bool showWorldWide;

  const CountryListView({
    Key? key,
    required this.onSelect,
    required this.fontFamily,
    required this.buildSearch,
    this.exclude,
    this.favorite,
    this.countryFilter,
    this.showPhoneCode = false,
    this.countryListTheme,
    this.searchAutofocus = false,
    this.showWorldWide = false,
  })  : assert(
          exclude == null || countryFilter == null,
          'Cannot provide both exclude and countryFilter',
        ),
        super(key: key);

  @override
  _CountryListViewState createState() => _CountryListViewState();
}

class _CountryListViewState extends State<CountryListView> {
  final CountryService _countryService = CountryService();

  late List<Country> _countryList;
  late List<Country> _filteredList;
  List<Country>? _favoriteList;
  late TextEditingController _searchController;
  late bool _searchAutofocus;

  @override
  void initState() {
    super.initState();
    _searchController = TextEditingController();

    _countryList = _countryService.getAll();

    _countryList = countryCodes.map((country) => Country.from(json: country)).toList();

    //Remove duplicates country if not use phone code
    if (!widget.showPhoneCode) {
      final ids = _countryList.map((e) => e.countryCode).toSet();
      _countryList.retainWhere((country) => ids.remove(country.countryCode));
    }

    if (widget.favorite != null) {
      _favoriteList = _countryService.findCountriesByCode(widget.favorite!);
    }

    if (widget.exclude != null) {
      _countryList.removeWhere(
        (element) => widget.exclude!.contains(element.countryCode),
      );
    }

    if (widget.countryFilter != null) {
      _countryList.removeWhere(
        (element) => !widget.countryFilter!.contains(element.countryCode),
      );
    }

    _filteredList = <Country>[];
    if (widget.showWorldWide) {
      _filteredList.add(Country.worldWide);
    }
    _filteredList.addAll(_countryList);

    _searchAutofocus = widget.searchAutofocus;
  }

  @override
  Widget build(BuildContext context) {
    final String searchLabel =
        CountryLocalizations.of(context)?.countryName(countryCode: 'search') ?? "Select country";

    return Column(
      children: <Widget>[
        const SizedBox(height: 25),
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 12),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                searchLabel,
                style: TextStyle(
                  color: const Color.fromRGBO(63, 63, 63, 1),
                  fontSize: 14,
                  fontWeight: FontWeight.w500,
                  fontFamily: widget.fontFamily,
                ),
              ),
              const SizedBox(width: 5),
              Flexible(child: widget.buildSearch(_filterSearchResults)),
            ],
          ),
        ),
        const SizedBox(height: 15),
        Expanded(
          child: ListView(
            padding: const EdgeInsets.symmetric(horizontal: 12),
            children: [
              if (_favoriteList != null) ...[
                ..._favoriteList!.map<Widget>((currency) => _listRow(currency)).toList(),
                const Padding(
                  padding: EdgeInsets.symmetric(horizontal: 20.0),
                  child: Divider(thickness: 1),
                ),
              ],
              ..._filteredList.map<Widget>((country) => _listRow(country)).toList(),
            ],
          ),
        ),
      ],
    );
  }

  Widget _listRow(Country country) {
    final TextStyle _textStyle = widget.countryListTheme?.textStyle ?? _defaultTextStyle;

    final bool isRtl = Directionality.of(context) == TextDirection.rtl;

    return Material(
      // Add Material Widget with transparent color
      // so the ripple effect of InkWell will show on tap
      color: Colors.transparent,
      child: InkWell(
        onTap: () {
          country.nameLocalized = CountryLocalizations.of(context)
              ?.countryName(countryCode: country.countryCode)
              ?.replaceAll(RegExp(r"\s+"), " ");
          widget.onSelect(country);
          Navigator.pop(context);
        },
        child: Container(
          margin: const EdgeInsets.only(bottom: 10),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(5),
            border: Border.all(color: const Color.fromRGBO(228, 228, 228, 1)),
          ),
          padding: const EdgeInsets.symmetric(vertical: 16, horizontal: 20),
          alignment: AlignmentDirectional.center,
          child: Row(
            children: [
              // Visibility(
              //   visible: selected,
              //   maintainSize: true,
              //   maintainAnimation: true,
              //   maintainState: true,
              //   child: SvgIconWrapper(
              //     color: context.colors.primary1,
              //     size: 24,
              //     iconPath: Res.check,
              //   ),
              // ),
              // const SizedBox(width: 6),
              Flexible(
                child: Text(
                  CountryLocalizations.of(context)
                          ?.countryName(countryCode: country.countryCode)
                          ?.replaceAll(RegExp(r"\s+"), " ") ??
                      country.name,
                  style: TextStyle(
                    color: const Color.fromRGBO(63, 63, 63, 1),
                    fontSize: 12,
                    fontWeight: FontWeight.w500,
                    fontFamily: widget.fontFamily,
                  ),
                ),
              ),
              const SizedBox(width: 8),
              Text(
                '${isRtl ? '' : '+'}${country.phoneCode}${isRtl ? '+' : ''}',
                style: TextStyle(
                  color: Colors.black54,
                  fontSize: 12,
                  fontWeight: FontWeight.w500,
                  fontFamily: widget.fontFamily,
                ),
              ),
            ],
          ),
        ),
        // child: Padding(
        //   padding: const EdgeInsets.symmetric(vertical: 15),
        //   child: Row(
        //     children: <Widget>[
        //       const SizedBox(width: 12),
        //       Flexible(
        //         child: Text(
        //           CountryLocalizations.of(context)
        //                   ?.countryName(countryCode: country.countryCode)
        //                   ?.replaceAll(RegExp(r"\s+"), " ") ??
        //               country.name,
        //           style: TextStyle(
        //             color: Colors.black,
        //             fontSize: 12,
        //             fontWeight: FontWeight.w500,
        //             fontFamily: widget.fontFamily,
        //           ),
        //         ),
        //       ),
        //       if (widget.showPhoneCode && !country.iswWorldWide) ...[
        //         const SizedBox(width: 15),
        //         SizedBox(
        //           width: 45,
        //           child: Text(
        //             '${isRtl ? '' : '+'}${country.phoneCode}${isRtl ? '+' : ''}',
        //             style: TextStyle(
        //               color: Colors.black54,
        //               fontSize: 12,
        //               fontWeight: FontWeight.w500,
        //               fontFamily: widget.fontFamily,
        //             ),
        //           ),
        //         ),
        //         const SizedBox(width: 5),
        //       ]
        //     ],
        //   ),
        // ),
      ),
    );
  }

  Widget _flagWidget(Country country) {
    final bool isRtl = Directionality.of(context) == TextDirection.rtl;
    return SizedBox(
      // the conditional 50 prevents irregularities caused by the flags in RTL mode
      width: isRtl ? 50 : null,
      child: Text(
        country.iswWorldWide ? '\uD83C\uDF0D' : Utils.countryCodeToEmoji(country.countryCode),
        style: TextStyle(
          fontSize: widget.countryListTheme?.flagSize ?? 25,
        ),
      ),
    );
  }

  void _filterSearchResults(String query) {
    List<Country> _searchResult = <Country>[];
    final CountryLocalizations? localizations = CountryLocalizations.of(context);

    if (query.isEmpty) {
      _searchResult.addAll(_countryList);
    } else {
      _searchResult = _countryList.where((c) => c.startsWith(query, localizations)).toList();
    }

    setState(() => _filteredList = _searchResult);
  }

  TextStyle get _defaultTextStyle => const TextStyle(fontSize: 16);
}
